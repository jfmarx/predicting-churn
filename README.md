# Predicting Churn

Understand what kinds of usage predict a higher risk for churn.

## Data Dictionary

| Events Table 	|  	|
|-	|-	|
|     request_id 	|     The unique ID of each event    	|
|  account_id 	|     The ID of the account that the user who triggered the   event is in     	|
|  user_id 	| The ID of the user who triggered the event 	|
| hasTyped  	| Boolean value demarcating whether a user typed   during the document edit. Users who didn't type either pasted text, deleted   text, or made formatting changes  	|
|     bodyWordCountStart     	| Word count of the document before the document edit 	|
|     bodyWordCountEnd     	| Word count of the document after the document edit 	|
|     documentId     	| Unique ID of the document where the edit occurred 	|
|     browser     	| Browser used by user when edit occurred 	|
|     device_platform     	| Operating system of the user's computer 	|
| event_time  	| Time (UTC) when the document_edit event occurred 	|
| editNumber  	| The ordinal number of the edit; a new edit is triggered   whenever the user pauses in the course of writing  	|


| Account Churn     	|        	|
|-	|-	|
| account_id     	|     The ID of an account. This can be joined to the account_id field in the document   edit file. Note that every account usually contains multiple users and   events.     	|
|  churned 	| Marks whether an account churned, where True   = churned and False = remained a customer 	|
|  company_name 	            | The human-readable display name of the company associated with   each account_id  	|